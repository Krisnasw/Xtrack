package com.eisbetech.xtrack.ui.views;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.eisbetech.xtrack.R;
import com.eisbetech.xtrack.data.model.ApiResponse;
import com.eisbetech.xtrack.data.remote.ApiClient;
import com.google.android.gms.samples.vision.barcodereader.BarcodeCapture;
import com.google.android.gms.samples.vision.barcodereader.BarcodeGraphic;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.zxing.Result;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.belvi.mobilevisionbarcodescanner.BarcodeRetriever;

public class MainActivity extends AppCompatActivity implements BarcodeRetriever, SurfaceHolder.Callback {

    //    @BindView(R.id.frameScan)
//    Fragment frameLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.linear)
    LinearLayout linearLayout;
    @BindView(R.id.surfaceView)
    SurfaceView surfaceView;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    Camera camera;
    SurfaceHolder surfaceHolder;
    Camera.PictureCallback rawCallback;
    Camera.ShutterCallback shutterCallback;
    Camera.PictureCallback jpegCallback;

    BarcodeCapture barcodeCapture;
    private SweetAlertDialog swal;
    String filename;
    String barcodeData = "";
    File dataFile;
    Unbinder unbinder;
    Uri imageLoc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        ButterKnife.bind(this);
        unbinder = ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("XTrack");
        toolbar.setTitleTextColor(Color.WHITE);

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        initScannerView();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Info")
                                .setContentText("Camera Tidak Diizinkan")
                                .show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {/* ... */}
                }).check();

        initScannerView();

        if (Build.VERSION.SDK_INT >= 23) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }

        btnSubmit.setVisibility(View.GONE);
        btnSubmit.setBackgroundColor(Color.RED);
        btnSubmit.setText("DISABLED");
    }

    private void initScannerView() {
        barcodeCapture = (BarcodeCapture) getSupportFragmentManager().findFragmentById(R.id.frameScan);
        barcodeCapture.setRetrieval(this);

        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        jpegCallback = new Camera.PictureCallback() {

            @Override
            public void onPictureTaken(byte[] data, Camera arg1) {
                // TODO Auto-generated method stub
                File pictureFileDir = getDir();

                if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {

                    Log.d(MainActivity.class.getSimpleName(),
                            "Can't create directory to save image.");
                    Toast.makeText(MainActivity.this,
                            "Can't create directory to save image.",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
                String date = dateFormat.format(new Date());
                String photoFile = "Picture_" + date + ".jpg";

                filename = pictureFileDir.getPath() + File.separator
                        + photoFile;

                imageLoc = Uri.parse(filename);
                dataFile = new File(String.valueOf(imageLoc));

                try {
                    FileOutputStream fos = new FileOutputStream(dataFile);
                    fos.write(data);
                    fos.close();
                    Toast.makeText(MainActivity.this,
                            "New Image saved:" + photoFile, Toast.LENGTH_LONG)
                            .show();
                } catch (Exception error) {
                    Log.d(MainActivity.class.getSimpleName(), "File" + filename
                            + "not saved: " + error.getMessage());
                    Toast.makeText(MainActivity.this,
                            "Image could not be saved.", Toast.LENGTH_LONG).show();
                }
            }

        };

        shutterCallback = new Camera.ShutterCallback() {

            @Override
            public void onShutter() {
                // TODO Auto-generated method stub
                Log.d(MainActivity.class.getSimpleName(), "onShutter'd");
            }
        };

        rawCallback = new Camera.PictureCallback() {

            @Override
            public void onPictureTaken(byte[] arg0, Camera arg1) {
                // TODO Auto-generated method stub
                Log.d(MainActivity.class.getSimpleName(), "onPictureTaken - raw");
            }

        };

    }

    private static File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, "Xtrack");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void captureImage() {
        // TODO Auto-generated method stub
        camera.takePicture(shutterCallback, rawCallback, jpegCallback);

        btnSubmit.setVisibility(View.VISIBLE);
        btnSubmit.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        btnSubmit.setText("Upload");
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swal = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                swal.setTitleText("Loading");
                swal.setContentText("Please wait...");
                swal.setCancelable(false);
                swal.show();

                RequestBody barcode = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(barcodeData));
                RequestBody image = RequestBody.create(MediaType.parse("image/*"), dataFile);
                MultipartBody.Part customerImg = MultipartBody.Part.createFormData("image", dataFile.getName(), image);

                doUpload(barcode, customerImg);
            }
        });
    }

    private void doUpload(RequestBody barcode, MultipartBody.Part customerImg) {
        ApiClient.get(this).uploadData(barcode, customerImg).enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                ApiResponse resp = response.body();

                if (resp.getStatus() == "true") {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(MainActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Success")
                            .setContentText(resp.getMessage())
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    startActivity(new Intent(MainActivity.this, MainActivity.class));
                                    finish();
                                }
                            })
                            .show();
                } else {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                try {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText(t.getMessage())
                            .show();
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void start_camera()
    {
        try {
            camera = Camera.open();
        } catch (RuntimeException e) {
            Log.e(MainActivity.class.getSimpleName(), "init_camera: " + e);
            return;
        }

        Camera.Parameters param;
        param = camera.getParameters();
        param.setPreviewFrameRate(20);
        param.setPreviewSize(176, 144);
        camera.setParameters(param);
        try {
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
        } catch (Exception e) {
            Log.e(MainActivity.class.getSimpleName(), "init_camera: " + e);
            return;
        }
    }

    private File getOutputMediaFile() {
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "XTrack");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("XTrack", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        dataFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void onRetrieved(final Barcode barcode) {
        Log.d(MainActivity.class.getSimpleName(), "Barcode read: " + barcode.displayValue);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                barcodeCapture.stopScanning();
                barcodeData = barcode.displayValue;
                surfaceView.setVisibility(View.VISIBLE);
                start_camera();
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            captureImage();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onRetrievedMultiple(final Barcode closetToClick, final List<BarcodeGraphic> barcodeGraphics) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                barcodeCapture.stopScanning();
                String message = "Code selected : " + closetToClick.displayValue + "\n\nother " +
                        "codes in frame include : \n";
                for (int index = 0; index < barcodeGraphics.size(); index++) {
                    Barcode barcode = barcodeGraphics.get(index).getBarcode();
                    message += (index + 1) + ". " + barcode.displayValue + "\n";
                }
            }
        });
    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onRetrievedFailed(String reason) {

    }

    @Override
    public void onPermissionRequestDenied() {

    }
}