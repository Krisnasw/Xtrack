package com.eisbetech.xtrack.data.remote;

import com.eisbetech.xtrack.data.model.ApiResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @POST("upload")
    @Multipart
    Call<ApiResponse> uploadData(
                @Part("barcode") RequestBody barcode,
                @Part MultipartBody.Part image
            );

}
